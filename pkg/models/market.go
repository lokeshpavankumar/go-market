package models

import (
	"go-market/pkg/config"

	"github.com/jinzhu/gorm"
)

var db *gorm.DB

type Company struct {
	gorm.Model
	Name        string  `json:"name"`
	TotalShares int     `json:"totalShares"`
	Price       float64 `json:"price"`
}

type Broker struct {
	gorm.Model
	InvestorName string  `json:"investorName"`
	Funds        float64 `json:"funds"`
}

type Demat struct {
	gorm.Model
	InvestorName string `json:"investorName"`
	CompanyName  string `json:"companyName"`
	Shares       int    `json:"shares"`
}

func init() {
	config.Connect()
	db = config.GetDB()
	db.AutoMigrate(&Company{}, &Broker{}, &Demat{})
}

func GetAllCompanies() []Company {
	var Company []Company
	db.Find(&Company)

	return Company
}

func (d *Demat) CreateTransaction() *Demat {
	db.NewRecord(d)
	db.Create(&d)
	return d
}

func (d *Demat) UpdateTransaction() *Demat {
	db.Model(&Demat{}).Where("investor_name = ? AND company_name = ?", d.InvestorName, d.CompanyName).Update("shares", d.Shares)
	return d
}

func GetCompanyByName(CompanyName string) int64 {
	var Company []Company
	result := db.Where("name = ?", CompanyName).First(&Company)

	return result.RowsAffected
}

func CheckStockPresence(Investor string, CommpanyName string) int64 {
	var Demat []Demat
	result := db.Where("investor_name = ? AND company_name = ?", Investor, CommpanyName).First(&Demat)

	return result.RowsAffected
}
