package controllers

import (
	"encoding/json"
	"go-market/pkg/models"
	"go-market/pkg/utils"
	"net/http"
)

func GetCompanies(w http.ResponseWriter, r *http.Request) {
	newCompanies := models.GetAllCompanies()

	res, _ := json.Marshal(newCompanies)
	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK) // 200 Ok

	w.Write(res)
}

func BuyTransaction(w http.ResponseWriter, r *http.Request) {
	CreateTransaction := &models.Demat{}

	utils.ParseBody(r, CreateTransaction)

	RowsAffected := models.GetCompanyByName(CreateTransaction.CompanyName)

	if RowsAffected < 1 {
		w.WriteHeader(http.StatusBadRequest) // 400 Bad Request
		return
	}

	b := CreateTransaction.CreateTransaction()

	res, _ := json.Marshal(b)

	w.WriteHeader(http.StatusAccepted) // 202 Accepted
	w.Write(res)
}

func SellTransaction(w http.ResponseWriter, r *http.Request) {
	SellTransaction := &models.Demat{}

	utils.ParseBody(r, SellTransaction)

	RowsAffected := models.CheckStockPresence(SellTransaction.InvestorName, SellTransaction.CompanyName)

	if RowsAffected < 1 {
		w.WriteHeader(http.StatusBadRequest) // 400 Bad Request
		return
	}

	s := SellTransaction.UpdateTransaction()

	res, _ := json.Marshal(s)

	w.WriteHeader(http.StatusAccepted) // 202 Accepted
	w.Write(res)

}
