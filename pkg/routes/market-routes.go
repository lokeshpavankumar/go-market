package routes

import (
	"go-market/pkg/controllers"

	"github.com/gorilla/mux"
)

var MarketRoutes = func(router *mux.Router) {
	router.HandleFunc("/companies/", controllers.GetCompanies).Methods("GET")
	router.HandleFunc("/buy/", controllers.BuyTransaction).Methods("POST")
	router.HandleFunc("/sell/", controllers.SellTransaction).Methods("POST")
}
